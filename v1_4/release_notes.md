## Release Notes 1.4

### New Features

*   Provisioning

*   It is now possible to initialize the SDK without domain key and secret
*   Verify domain availability

*   Access Notification Delegate

*   Get notified about session expiration and SDK version not supported

*   Session Renewal

*   Added option for renewing the expired session by using the method “renewExpiredSession”.
*   Added option for automatic renewal of the expired session using the next global config “isAutoRenewSession”.

*   Group

*   Group Service is a new addition, which enables you to manage and view your groups
*   You can now send Chat messages to a group using the Chat service

*   Download Path Management

*   Enables you to define the path and file name for downloaded files. Download path management is possible via relevant service settings such as: Chat and Group.

*   Call Service

*   Added—the ability to check whether a GSM call is active using “isInGSMCall”

### Fixes

*   Prevent incoming message duplication
*   While in VoIP call, going into “Hold” is disabled when receiving a GSM call

### Breaking Changes

The following syntax changes have been made in this version. Using the old syntax will result in unexpected results, and generate errors.

*   Chat service – “markAsReceived”
*   Chat service Settings – removed “baseDownloadPathFile” , “thumbnailFilePrefix”. You should use “downloadPathBuilder” protocol
*   Chat service notification delegate – “onMessageReceived” also passes the recipient type
*   Notification service – “handleRemoteNotification”
*   Presence service – “getPresenceForRecords”
*   Subscriber provision – all methods
*   Kandy – initialization and reset methods
