## Release Notes 1.5

## V1.5.1

### Fixes

*   Connection state shows “Disconnected” when no data channel was defined

## V1.5

### New Features:

*   Data Channel

*   KandySDK now maintains a data channel to enable you to receive events while application is running (foreground/background)
*   Set “keepAliveTimeout:handler:” in your application (see Quick Start Guide)

*   Access

*   It is now possible to login with a user access token
*   Added an API to set as handler for “setKeepAliveTimeout:Handler”
*   You can now get both the connection state and the registration state

*   Call

*   Added property: “isAudioOnlyCall”, represents calls that cannot be upgraded to a video call
*   New call option added: “createSIPTrunkCall”
*   New notifications added: gotPendingVoiceMailMessage, availableAudioOutputChanged
*   "KandyCallTerminationReasonProtocol" now has a reason string and an error code

*   DownloadPathBuilder

*   DownloadPathBuilder is now divided into two separate protocols: Chat and Group.

*   Chat: defines the path for chat-related files
*   Group: defines the path for group management files

*   Profile

*   New Profile service was added: set/get the user device’s profile

*   Group

*   KandyGroupParticipantLeft – added “admins” (NSArray) – optional – if an admin left the group, will contain the new admins
*   KandyGroupUpdated – added “updater” (KandyRecord)

*   KandySubscriberProvision

*   New methods added:

*   getUserDetails – get the user details by the user id
*   provisionUser – provision a new user
*   removeUser – deletes user from the system
*   addUserDevice – associate device ta provisioned user
*   removeUserDevice – disassociate device from a provisioned user

*   EventProtocol

*   New event type was added: EKandyEventType_pendingVoiceMail
*   Added UTC timestamp correction tKandySession

*   UserInfo

*   Added new field to dataByKey:

*   kKandyUserInfoPhoneNumber
*   kKandyUserInfoCountryCode
*   kKandyUserInfoFirstName
*   kKandyUserInfoLastName
*   kKandyUserInfoEmail
*   kKandyUserInfoVirtualNumber

### Fixes:

*   Access

*   endCallSucceeded is not called when terminating a call
*   Some delegates were retained thus preventing them from being released
*   By default, group media was being saved by the sender id and not by the group id
*   Not sending notification for pending missed call
*   When updating the group image, imageAbsolutePath does not get updated
*   Various bug fixes

### Breaking Changes:

The following syntax changes have been made in this version. Using the old syntax will result in unexpected results, and generate errors.

*   Call

*   getAvailableAudioOutputs was moved to KandyCallService
*   All calls creation now receives the caller KandyRecord as well as the callee
*   createVoipCall also receives EKandyOutgingCallOptions
*   EKandyCallState – Added “sessionProgress”
*   EKandyCallState – these are the common scenarios for incoming/outgoing call:

*   Incoming - EKandyCallState_ringing -> EKandyCallState_talking -> EKandyCallState_terminated
*   Outgoing – EkandyCallState_dialing -> EKandyCallState_sessionProgress (optional) -> EKandyCallState_ringing -> EKandyCallState_talking -> EKandyCallState_terminated

*   ECallTerminationReason was removed – see “call” under new features

*   KandyPushNotificationService

*   enableRemoteNotificationsWithToken – now also receives “isSandbox” (BOOL)

*   KandySessionManagement + KandyUserInfo

*   loggedInUser – renamed to “currentUser”
*   KandySubscriberProvision:

*   validateCode – validate OTP
*   validateAndProvision – validate OTP and provision a user
*   deactivateWithResponseCallback – deactivates the current device
